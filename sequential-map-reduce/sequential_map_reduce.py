# sequential-map-reduce.py
# Author: Brigham Moll
# Description: Counts the number of times each word appears in a text.
# Does not display proposition counts.
# Does this sequentially, aka one thread.

# Used to find total time of execution.
from datetime import datetime

# Regex used to find all words in file.
import re

# Constants

# Name of dataset file.
dataSetFileName = "sampleDataset.txt"

# Propositions to skip when displaying word counts.
propositionList = ['a', 'an', 'and', 'are', 'as', 'be', 'by', 'for', 'if', 'in', 'is', 'it', 'of', 'or', 'py', 'rst', 'that', 'the', 'to', 'with', 's', 't']

# Methods

#Imports text from a file and makes a list of strings from it. Returns the list.
def loadStringList():
    #Load the sample dataset text file.
    dataSetFile = open(dataSetFileName, "r")

    #If file opened correctly, read the data from it.
    dataSetContents = ""
    if dataSetFile.mode == 'r':
        dataSetContents = dataSetFile.read()

    dataSetFile.close()

    # Set all the text to lowercase.
    dataSetContents = dataSetContents.lower()

    # Place all words from file into a list and return it.
    listOfStrings = re.findall(r'\w+', dataSetContents)
    
    return listOfStrings

#Calculates how often words appear in a list of strings. Returns a dict with that info.
def calculateFrequencyOfWords(listOfStrings):
    # Create a place to store words and their frequencies.
    wordDictionary = {}

    # Loop through list. 
    # When a new word is found, add it as a new key with a value of 1.
    # When a word is found again, increment its value by 1.
    for word in listOfStrings:
        if word not in wordDictionary:
            wordDictionary[word] = 1
        else:
            wordDictionary[word] += 1

    # Return the list of words with their frequency counts.
    return wordDictionary

#Starting point for script.
def runProgram():
    #Load the words file.
    stringList = loadStringList()

    #Mark the start time.
    startTime = datetime.now()

    #Get the frequency of each word in 'sampleDataset.txt'.
    wordDictionary = calculateFrequencyOfWords(stringList)

    #Processing is finished, record time that it took to process problem.
    totalTimeRequired = datetime.now() - startTime

    #Print to the console all of the words with their frequencies next to them, besides any in proposition list.
    wordFrequenciesOutput = ""
    for word in wordDictionary:
        if word not in propositionList:
            wordFrequenciesOutput = wordFrequenciesOutput + "\n" + word + " " + str(wordDictionary[word])

    print(wordFrequenciesOutput)    

    #Print to the console how much time was needed for processing.
    print("Time used to complete problem:", totalTimeRequired)

    # Save output to a file.
    outputFile = open("seqMapReduceOutput.txt", "w+")
    outputFile.write(wordFrequenciesOutput)
    outputFile.write("\nTime used to complete problem: " + str(totalTimeRequired))
    outputFile.close()

# Run the program.
runProgram()