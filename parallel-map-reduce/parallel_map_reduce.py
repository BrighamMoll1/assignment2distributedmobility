# parallel-map-reduce.py
# Author: Brigham Moll
# Description: Counts the number of times each word appears in a text.
# Does not display proposition counts.
# Done in parallel using multiprocessing library.

# Used to find total time of execution.
from datetime import datetime

# Regex used to find all words in file.
import re

# Used for parallel computing.
import multiprocessing
import itertools

# Constants

# Name of dataset file.
sampleDataSetFileName = "sampleDataset.txt"

# Propositions to skip when displaying word counts.
propositionList = ['a', 'an', 'and', 'are', 'as', 'be', 'by', 'for', 'if', 'in', 'is', 'it', 'of', 'or', 'py', 'rst', 'that', 'the', 'to', 'with', 's', 't']

# How many threads to use to do work.
numberOfThreads = 8

# Global Variables

# Places to put the workloads after being divided up.
workload1 = []
workload2 = []
workload3 = []
workload4 = []
workload5 = []
workload6 = []
workload7 = []
workload8 = []

# Pool of threads for parallel processing.
if __name__ == '__main__':
    parallelPool = multiprocessing.Pool(numberOfThreads)

# Overall list of words and their frequencies.
finalWordFreqDict = {}

# Methods

#Imports text from a file and makes a list of strings from it. Returns the list.
def loadStringList(dataSetFileName):
    #Load the sample dataset text file.
    dataSetFile = open(dataSetFileName, "r")

    #If file opened correctly, read the data from it.
    dataSetContents = ""
    if dataSetFile.mode == 'r':
        dataSetContents = dataSetFile.read()

    dataSetFile.close()

    # Set all the text to lowercase.
    dataSetContents = dataSetContents.lower()

    # Place all words from file into a list and return it.
    listOfStrings = re.findall(r'\w+', dataSetContents)
    
    return listOfStrings

#Calculates how often words appear in a list of strings. Returns a dict with that info.
def calculateFrequencyOfWords(listOfStrings):
    # Print thread has started its work.
    print(multiprocessing.current_process().name, "has started.")

    # Create a place to store words and their frequencies.
    wordDictionary = {}

    # Loop through list. 
    # When a new word is found, add it as a new key with a value of 1.
    # When a word is found again, increment its value by 1.
    for word in listOfStrings:
        if word not in wordDictionary:
            wordDictionary[word] = 1
        else:
            wordDictionary[word] += 1

    # Print thread has completed its work.
    print(multiprocessing.current_process().name, "has finished.")

    # Return the dictionary of words with their frequency counts.
    return wordDictionary

#Divides up the listOfStrings from the loaded file into 8 work loads.
def divideWork(listOfStrings):
    # Try to divide work into 8 pieces.
    nextLoadAssigned = 1
    for word in listOfStrings:
        if nextLoadAssigned == 1:
            workload1.append(word)
            nextLoadAssigned += 1
        elif nextLoadAssigned == 2:
            workload2.append(word)
            nextLoadAssigned += 1
        elif nextLoadAssigned == 3:
            workload3.append(word)
            nextLoadAssigned += 1
        elif nextLoadAssigned == 4:
            workload4.append(word)
            nextLoadAssigned += 1
        elif nextLoadAssigned == 5:
            workload5.append(word)
            nextLoadAssigned += 1
        elif nextLoadAssigned == 6:
            workload6.append(word)
            nextLoadAssigned += 1
        elif nextLoadAssigned == 7:
            workload7.append(word)
            nextLoadAssigned += 1
        else:
            workload8.append(word)
            nextLoadAssigned = 1

# Use multiple threads to get the word frequencies and return them.
def mapReduce(listOfStrings):
    if __name__ == '__main__':
        # Divide work for 8 threads. (map)
        divideWork(listOfStrings)
        dividedWorkList = [workload1,workload2,workload3,workload4,workload5,workload6,workload7,workload8]

        # Use each thread to sum up frequencies of words. (reduce)
        mapResponses = parallelPool.map(calculateFrequencyOfWords, dividedWorkList, 1)

        # Put together frequencies from all threads.
        reduceAllFrequencies(mapResponses)
        #Result saved in global finalWordFreqDict variable.

# Put together frequencies from all threads.
def reduceAllFrequencies(mapResponses):
    for wordFreqDict in mapResponses:
        for word in wordFreqDict:
            if word not in finalWordFreqDict:
                finalWordFreqDict[word] = wordFreqDict[word]
            else:
                finalWordFreqDict[word] += wordFreqDict[word]

#Starting point for script.
def runProgram():
    if __name__ == '__main__':
        #Load the words file.
        stringList = loadStringList(sampleDataSetFileName)

        #Mark the start time.
        startTime = datetime.now()

        #Get a dictionary of word frequencies in a given file through parallel map reduce.
        wordFreqencies = mapReduce(stringList)

        #Processing is finished, record time that it took to process problem.
        totalTimeRequired = datetime.now() - startTime

        #Print to the console all of the words with their frequencies next to them, besides any in proposition list.
        wordFrequenciesOutput = ""
        for word in finalWordFreqDict:
            if word not in propositionList:
                wordFrequenciesOutput = wordFrequenciesOutput + "\n" + word + " " + str(finalWordFreqDict[word])

        print(wordFrequenciesOutput)    

        #Print to the console how much time was needed for processing.
        print("Time used to complete problem:", totalTimeRequired)

        # Save output to a file.
        outputFile = open("parallelMapReduceOutput.txt", "w+")
        outputFile.write(wordFrequenciesOutput)
        outputFile.write("\nTime used to complete problem: " + str(totalTimeRequired))
        outputFile.close()

# Run the program.
runProgram()